package com.r0cker.interstellar.listeners;

public interface OpenFragment {
    // Handles most button clicks on each fragment and is implemented in MapsActivity
    void openFrag(int which, int index);
}
