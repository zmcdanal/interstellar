package com.r0cker.interstellar.utilities;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.r0cker.interstellar.R;
import com.r0cker.interstellar.listeners.OpenFragment;
import com.r0cker.interstellar.objects.Beer;

import java.util.ArrayList;

@RequiresApi(api = Build.VERSION_CODES.M)
public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.CustomViewHolder> {

    // Various Variables
    private static final String TAG = "CustomAdapter";
    private final Context context;
    private final ArrayList<Beer> beers;
    private final OpenFragment mOpenFragmentListener;

    // Constructor
    public CustomAdapter(Context context, ArrayList<Beer> beers, OpenFragment openFragment) {
        this.context = context;
        this.beers = beers;
        this.mOpenFragmentListener = openFragment;
    }

    @NonNull
    @Override
    public CustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CustomViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_beer_layout, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final CustomViewHolder holder, final int position) {
        holder.itemView.setTag(position);
        holder.beerImage.setImageDrawable(whichImage(beers.get(position).getTitle()));
        holder.beerTitle.setText(beers.get(position).getTitle());
        holder.beerDescrip.setText(beers.get(position).getDescrip());
        holder.beerAbv.setText(beers.get(position).getAbv());
        holder.beerIngredients.setText(beers.get(position).getIngredients());

        holder.mapImgBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOpenFragmentListener.openFrag(10, (Integer)holder.itemView.getTag());
            }
        });

    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemCount() {
        return beers.size();
    }


    public static class CustomViewHolder extends RecyclerView.ViewHolder {

        private final ImageView beerImage;
        private final TextView beerTitle;
        private final TextView beerDescrip;
        private final TextView beerAbv;
        private final TextView beerIngredients;
        private final ImageButton mapImgBtn;

        public CustomViewHolder(View view) {
            super(view);

            beerImage = view.findViewById(R.id.beerImg);
            beerTitle = view.findViewById(R.id.beerTitleTV);
            beerDescrip = view.findViewById(R.id.descripTV);
            beerAbv = view.findViewById(R.id.abvTV);
            beerIngredients = view.findViewById(R.id.ingredientsTV);
            mapImgBtn = view.findViewById(R.id.mapImgBtn);
        }
    }

    public Drawable whichImage(String beer) {
        Drawable drawable = ContextCompat.getDrawable(context,R.drawable.no_image);
        switch (beer) {
            case "First Contact":
                drawable = ContextCompat.getDrawable(context,R.drawable.firstcontact);
                break;
            case "Ginger Colada":
                drawable = ContextCompat.getDrawable(context,R.drawable.gingercolada);
                break;
            case "Space Mule":
                drawable = ContextCompat.getDrawable(context,R.drawable.spacemule);
                break;
            case "Andromeda Ambrosia":
                break;
            case "Martian Mojito":
                drawable = ContextCompat.getDrawable(context,R.drawable.mojito);
                break;
            case "Starberry Sour":
                drawable = ContextCompat.getDrawable(context,R.drawable.starberry);
                break;
            case "Sunspot":
                drawable = ContextCompat.getDrawable(context,R.drawable.sunspot);
                break;
            case "Member Berries":
                drawable = ContextCompat.getDrawable(context,R.drawable.memberries);
                break;
            case "Nice Melons":
                break;
            case "Dark & Stormy":
                drawable = ContextCompat.getDrawable(context,R.drawable.darkstormy);
                break;
            case "Peach Me I'm Dreaming":
                drawable = ContextCompat.getDrawable(context,R.drawable.peachme);
                break;
            case "Born to Rum":
                break;
            case "Tequlia'n Me Smalls":
                break;
            case "Seltzer":
                break;
            default:
                Log.d(TAG, "CustomAdapter - whichImage: Error - Default was triggered");
                break;
        }

        return drawable;
    }
}


