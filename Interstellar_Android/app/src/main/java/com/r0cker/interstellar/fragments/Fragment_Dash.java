package com.r0cker.interstellar.fragments;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.r0cker.interstellar.R;
import com.r0cker.interstellar.listeners.OpenFragment;

public class Fragment_Dash extends Fragment implements View.OnClickListener,OnMapReadyCallback, GoogleMap.InfoWindowAdapter, GoogleMap.OnMarkerClickListener, GoogleMap.OnInfoWindowClickListener {

    // Various Variables
    private static final String TAG = "Fragment_Dash";
    private boolean infoWindowShowing = false;
    OpenFragment mOpenFragmentListener;

    public Fragment_Dash() {
    }

    public static Fragment_Dash newInstance() {

        Bundle args = new Bundle();

        Fragment_Dash fragment = new Fragment_Dash();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //attaching button listener
        if (context instanceof OpenFragment) {
            mOpenFragmentListener = (OpenFragment) context;
        }
        else {
            Log.e(TAG, "onAttach: " + context.toString() + " must implement OpenFragment.Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_dash, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if (getView() != null) {

            // Buttons are registered here
        Button aboutUsBtn = getView().findViewById(R.id.aboutUsBtn);
        Button ourBeersBtn = getView().findViewById(R.id.ourBeersBtn);
        Button podcastBtn = getView().findViewById(R.id.podcastBtn);
        Button contactUsBtn = getView().findViewById(R.id.contactUsBtn);
        Button developerBtn = getView().findViewById(R.id.developerBtn);

        aboutUsBtn.setOnClickListener(this);
        ourBeersBtn.setOnClickListener(this);
        podcastBtn.setOnClickListener(this);
        contactUsBtn.setOnClickListener(this);
        developerBtn.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
            switch (id) {
                case R.id.aboutUsBtn:
                    // To About Us Fragment
                    mOpenFragmentListener.openFrag(0, 0);
                    break;
                case R.id.ourBeersBtn:
                    // To Our Beers Fragment
                    mOpenFragmentListener.openFrag(1, 0);
                    break;
                case R.id.podcastBtn:
                    // To Podcast Fragment
                    mOpenFragmentListener.openFrag(2, 0);
                    break;
                case R.id.contactUsBtn:
                    // To Contact Us Fragment
                    mOpenFragmentListener.openFrag(3, 0);
                    break;
                case R.id.developerBtn:
                    // To About the Developer Fragment
                    mOpenFragmentListener.openFrag(4, 0);
                    break;
                default:
                    Log.d(TAG, "onClick: Error: default was triggered");
            }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        // Add a marker at brewery and move the camera
        LatLng alabaster = new LatLng(33.187689, -86.78175);
        googleMap.addMarker(new MarkerOptions().position(alabaster).title("Interstellar Ginger Beer & Exploration Co")
                .snippet("260A Regency Park Dr, Alabaster, AL 35007"));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(alabaster, 11f));

        // Map setup
        googleMap.setInfoWindowAdapter(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);
    }


    @Override
    public boolean onMarkerClick(Marker marker) {
        if (infoWindowShowing) {
            infoWindowShowing = false;
            marker.hideInfoWindow();
        } else {
            infoWindowShowing = true;
            marker.showInfoWindow();
        }
        return true;
    }

    @Override
    public View getInfoWindow(Marker marker) {
        return null;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Log.d(TAG, "onInfoWindowClick: SUp");
        String label = "Interstellar";
        String uriBegin = "geo:" + marker.getPosition().latitude + "," + marker.getPosition().longitude;
        String query = marker.getPosition().latitude + "," + marker.getPosition().longitude + "(" + label + ")";
        String encodedQuery = Uri.encode(query);
        String uriString = uriBegin + "?q=" + encodedQuery + "&z=16";
        Uri uri = Uri.parse(uriString);
        Intent intent = new Intent(android.content.Intent.ACTION_VIEW,uri);
        startActivity(intent);
    }

    @Override
    public View getInfoContents(Marker marker) {
        View contents = LayoutInflater.from(getContext())
                .inflate(R.layout.marker_info_window, null);

        ((TextView)contents.findViewById(R.id.title)).setText(marker.getTitle());
        ((TextView)contents.findViewById(R.id.address)).setText(marker.getSnippet());

        return contents;
    }
}
