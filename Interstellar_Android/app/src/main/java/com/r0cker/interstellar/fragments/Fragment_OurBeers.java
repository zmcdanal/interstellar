package com.r0cker.interstellar.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.r0cker.interstellar.R;
import com.r0cker.interstellar.listeners.OpenFragment;
import com.r0cker.interstellar.objects.Beer;
import com.r0cker.interstellar.utilities.CustomAdapter;

import java.util.ArrayList;

public class Fragment_OurBeers extends Fragment  {

    // Various Variables
    private static final String TAG = "Fragment_OurBeers";
    OpenFragment mOpenFragmentListener;
    RecyclerView recyclerView;

    public Fragment_OurBeers() {
    }

    public static Fragment_OurBeers newInstance(ArrayList<Beer> allBeers) {

        Bundle args = new Bundle();
        args.putSerializable(TAG, allBeers);
        Fragment_OurBeers fragment = new Fragment_OurBeers();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //attaching button listener
        if (context instanceof OpenFragment) {
            mOpenFragmentListener = (OpenFragment) context;
        } else {
            Log.e(TAG, "onAttach: " + context.toString() + " must implement OpenFragment.Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_our_beers, container, false);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null && getArguments() != null) {

            final ArrayList<Beer> allBeers = (ArrayList<Beer>) getArguments().getSerializable(TAG);

            // Dismiss Fragment back to dashboard
            ImageButton dismissBtn = getView().findViewById(R.id.dismissBtn);
            dismissBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStackImmediate();
                    }
                }
            });

            // Recycle view setup
            recyclerView = (RecyclerView) getView().findViewById(R.id.rv);
            CustomAdapter recyclerAdapter = new CustomAdapter(getContext(), allBeers, mOpenFragmentListener);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
            recyclerView.setAdapter(recyclerAdapter);

        }
    }
}
