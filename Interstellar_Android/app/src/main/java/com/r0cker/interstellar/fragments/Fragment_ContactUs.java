package com.r0cker.interstellar.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.r0cker.interstellar.R;
import com.r0cker.interstellar.listeners.OpenFragment;

public class Fragment_ContactUs extends Fragment implements View.OnClickListener {

    // Various Variables
    private static final String TAG = "Fragment_ContactUs";
    OpenFragment mOpenFragmentListener;

    public Fragment_ContactUs() {
    }

    public static Fragment_ContactUs newInstance() {

        Bundle args = new Bundle();

        Fragment_ContactUs fragment = new Fragment_ContactUs();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //attaching button listener
        if (context instanceof OpenFragment) {
            mOpenFragmentListener = (OpenFragment) context;
        } else {
            Log.e(TAG, "onAttach: " + context.toString() + " must implement OpenFragment.Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {

            ImageButton phoneImgBtn = getView().findViewById(R.id.phoneImgBtn);
            ImageButton emailImgBtn = getView().findViewById(R.id.emailImgBtn);
            ImageButton facebookImgBtn = getView().findViewById(R.id.facebookImgBtn);
            ImageButton instagramImgBtn = getView().findViewById(R.id.instagramImgBtn);
            ImageButton dismissBtn = getView().findViewById(R.id.dismissBtn);

            phoneImgBtn.setOnClickListener(this);
            emailImgBtn.setOnClickListener(this);
            facebookImgBtn.setOnClickListener(this);
            instagramImgBtn.setOnClickListener(this);
            dismissBtn.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        switch (id) {
            case R.id.phoneImgBtn:
                // Open Phone
                mOpenFragmentListener.openFrag(6, 0);
                break;
            case R.id.emailImgBtn:
                // Open Email
                mOpenFragmentListener.openFrag(7, 0);
                break;
            case R.id.facebookImgBtn:
                // Open Facebook
                mOpenFragmentListener.openFrag(8, 0);
                break;
            case R.id.instagramImgBtn:
                // Open Instagram
                mOpenFragmentListener.openFrag(9, 0);
                break;
            case R.id.dismissBtn:
                // Dismiss Fragment back to dashboard
                if (getFragmentManager() != null) {
                    getFragmentManager().popBackStackImmediate();
                }
                break;
            default:
                Log.d(TAG, "onClick: Error - Default was triggered");
                break;
        }
    }
}
