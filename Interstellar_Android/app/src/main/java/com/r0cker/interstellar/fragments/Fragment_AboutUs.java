package com.r0cker.interstellar.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.r0cker.interstellar.R;
import com.r0cker.interstellar.listeners.OpenFragment;

public class Fragment_AboutUs extends Fragment {

    // Various Variables
    private static final String TAG = "Fragment_AboutUs";
    OpenFragment mOpenFragmentListener;

    public Fragment_AboutUs() {
    }

    public static Fragment_AboutUs newInstance() {

        Bundle args = new Bundle();

        Fragment_AboutUs fragment = new Fragment_AboutUs();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        //attaching button listener
        if (context instanceof OpenFragment) {
            mOpenFragmentListener = (OpenFragment) context;
        } else {
            Log.e(TAG, "onAttach: " + context.toString() + " must implement OpenFragment.Listener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about_us, container, false);
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getView() != null) {
            // Dismiss Fragment back to dashboard
            ImageButton dismissBtn = getView().findViewById(R.id.dismissBtn);
            dismissBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (getFragmentManager() != null) {
                        getFragmentManager().popBackStackImmediate();
                    }
                }
            });
        }
    }
}
