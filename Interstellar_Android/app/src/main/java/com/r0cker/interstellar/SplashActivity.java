package com.r0cker.interstellar;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_layout);
        overridePendingTransition(R.anim.act_fadein, R.anim.act_fade_ou);

        // Beginning animation setup
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                    Intent profileIntent = new Intent(getApplicationContext(), MapsActivity.class);
                    startActivity(profileIntent);
                    finish();
            }
        }, 5000);
    }





}