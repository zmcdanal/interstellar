package com.r0cker.interstellar.objects;

import java.io.Serializable;
import java.util.ArrayList;

public class Beer implements Serializable {

    // Variables
    private String title;
    private String descrip;
    private String abv;
    private ArrayList<String> ingredients;
    private ArrayList<BeerLocation> locations;

    // Constructor
    public Beer(String title, String descrip, String abv, ArrayList<String> ingredients, ArrayList<BeerLocation> locations) {
        this.title = title;
        this.descrip = descrip;
        this.abv = abv;
        this.ingredients = ingredients;
        this.locations = locations;
    }

    // Getters
    public String getTitle() {
        return title;
    }

    public String getDescrip() {
        return descrip;
    }

    public String getAbv() {
        return abv;
    }

    public String getIngredients() {
        StringBuilder valueToReturn = new StringBuilder();
        for (int i = 0; i < ingredients.size(); i++) {
            String value = "• " + ingredients.get(i) + "\n";
            valueToReturn.append(value);
        }
        return valueToReturn.toString();
    }

    public ArrayList<BeerLocation> getLocations() {
        return locations;
    }

    // Setters
    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescrip(String descrip) {
        this.descrip = descrip;
    }

    public void setAbv(String abv) {
        this.abv = abv;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

    public void setLocations(ArrayList<BeerLocation> locations) {
        this.locations = locations;
    }
}
