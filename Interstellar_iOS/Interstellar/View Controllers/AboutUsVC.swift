//
//  AboutUsVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/3/20.
//

import UIKit

class AboutUsVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.view.addBackground(which: 0)
        
        // Various Method Calls
        setupNavBar()
        
        
    }
    
    // Setup for the top navigation bar
    func setupNavBar() {
        let image = UIImage(named: "logo")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit

        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#f8ddB0")
        self.navigationController?.navigationBar.tintColor = UIColor(hexString: "#CA4D3C")

        navigationItem.titleView = imageView
        let customImageBarBtn1 = UIBarButtonItem(
            image: UIImage(named: "dismiss.png")!.withRenderingMode(.alwaysOriginal),
            style: .plain, target: self, action: #selector(dismissPage(_:)))
        navigationItem.leftBarButtonItem = customImageBarBtn1
    }
    
    // Method called from top nav bar that dismisses page
    @objc func dismissPage(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Before page dismisses, change button background on DashVC from red back to default color
    override func viewWillDisappear(_ animated: Bool) {
        if let presenter = presentingViewController as? DashVC {
            presenter.squares[0].backgroundColor = UIColor(hexString: "#F8DDB0")
            presenter.buttons[0].setTitleColor(UIColor(hexString: "#F8DDB0"), for: .normal)
            presenter.selectionViews[0].isHidden = true
        }
    }
}
