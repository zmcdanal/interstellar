//
//  ContactUsVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/5/20.
//

import UIKit

class ContactUsVC: UIViewController {
    
    // Outlets
    @IBOutlet var connections: [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addBackground(which: 2)
        setupNavBar()
        // Do any additional setup after loading the view.
        
        for option in connections {
            let tap = UITapGestureRecognizer(target: self, action: #selector(ContactUsVC.contactOptionPressed))
            option.addGestureRecognizer(tap)
            option.isUserInteractionEnabled = true
        }
    }
    
    // Setup for the top navigation bar
    func setupNavBar() {
        let image = UIImage(named: "logo") 
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#f8ddB0")
        self.navigationController?.navigationBar.tintColor = UIColor(hexString: "#CA4D3C")

        navigationItem.titleView = imageView
        let customImageBarBtn1 = UIBarButtonItem(
            image: UIImage(named: "dismiss.png")!.withRenderingMode(.alwaysOriginal),
            style: .plain, target: self, action: #selector(dismissPage(_:)))
        navigationItem.leftBarButtonItem = customImageBarBtn1
    }
    
    // Method called from top nav bar that dismisses page
    @objc func dismissPage(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Before page dismisses, change button background on DashVC from red back to default color
    override func viewWillDisappear(_ animated: Bool) {
        if let presenter = presentingViewController as? DashVC {
            presenter.squares[3].backgroundColor = UIColor(hexString: "#F8DDB0")
            presenter.buttons[3].setTitleColor(UIColor(hexString: "#F8DDB0"), for: .normal)
            presenter.selectionViews[3].isHidden = true
        }
    }
    
    // Handles all button clicks on page
    @objc func contactOptionPressed(sender:UITapGestureRecognizer) {
        switch sender.view!.tag {
        case 0: // Phone
            if let phoneCallURL = URL(string: "tel://12053351363") {
                let application:UIApplication = UIApplication.shared
                if (application.canOpenURL(phoneCallURL)) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                }
            }
            break
        case 1: // Email
            let email = "InterstellarGinger@gmail.com"
            if let url = URL(string: "mailto:\(email)") {
              if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
              } else {
                UIApplication.shared.openURL(url)
              }
            }
            break
        case 2: // Facebook
            let facebookURL = URL(string: "fb://profile/978067088947339")
            if let facebookURL = facebookURL {
                if UIApplication.shared.canOpenURL(facebookURL) {
                    UIApplication.shared.open(facebookURL)
                } else {
                    if let url = URL(string: "http://facebook.com/interstellarginger") {
                        UIApplication.shared.open(url)
                    }
                }
            }
            break
        case 3: // Instagram
            let instagramHooks = "instagram://user?username=interstellargingerbeer"
            let instagramUrl = NSURL(string: instagramHooks)
            if UIApplication.shared.canOpenURL(instagramUrl! as URL) {
                UIApplication.shared.open(instagramUrl! as URL)
            } else {
              //redirect to safari because the user doesn't have Instagram
                UIApplication.shared.open(NSURL(string: "https://instagram.com/interstellargingerbeer?igshid=nyrswv0rvyyg")! as URL)
            }
            break
        default:
            print("ContactUs: Error with contactOptionPressed()")
        }
    }
    
}
