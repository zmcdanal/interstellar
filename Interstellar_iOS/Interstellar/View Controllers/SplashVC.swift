//
//  SplashVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/9/20.
//

import UIKit
import Lottie

class SplashVC: UIViewController {
    
    // Outlets
    @IBOutlet weak var splashAnim: AnimationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Animation setup
        splashAnim.loopMode = .loop
        splashAnim.animationSpeed = 0.5
        splashAnim.play()
        // Do any additional setup after loading the view.
    }
    
    // Segue to DashVC
    override func viewDidAppear(_ animated: Bool) {
            DispatchQueue.main.asyncAfter(deadline: .now() + 2.0) {
               
                //splash complete
                self.performSegue(withIdentifier: "toDash", sender: nil)
              
            }
        }

}
