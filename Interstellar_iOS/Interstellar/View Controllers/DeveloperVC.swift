//
//  DeveloperVC.swift
//  Interstellar
//
//  Created by Zakarias McDanal on 11/5/20.
//

import UIKit
import StoreKit

class DeveloperVC: UIViewController, SKProductsRequestDelegate, SKPaymentTransactionObserver {
    
    // Outlets
    @IBOutlet weak var mugshot: UIImageView!
    
    var myProduct: SKProduct?
    var index = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addBackground(which: 0)
        setupNavBar()
        // Do any additional setup after loading the view.
    }
    
    // Setup for the top navigation bar
    func setupNavBar() {
        let image = UIImage(named: "logo") //Your logo url here
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        
        // Or, Set to new colour for just this navigation bar
        self.navigationController?.navigationBar.barTintColor = UIColor(hexString: "#f8ddB0")
        self.navigationController?.navigationBar.tintColor = UIColor(hexString: "#CA4D3C")
        
        navigationItem.titleView = imageView
        let customImageBarBtn1 = UIBarButtonItem(
            image: UIImage(named: "dismiss.png")!.withRenderingMode(.alwaysOriginal),
            style: .plain, target: self, action: #selector(dismissPage(_:)))
        navigationItem.leftBarButtonItem = customImageBarBtn1
    }
    
    // Method called from top nav bar that dismisses page
    @objc func dismissPage(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    
    // Handles button to show developer's web business card
    @IBAction func contactMeClicked(_ sender: UIButton) {
        if let url = URL(string: "https://linqapp.com/zmcdanal") {
            UIApplication.shared.open(url)
        }
    }
    
    // Hanldes tip option buttons
    @IBAction func tipOptionClicked(_ sender: UIButton) {
        switch sender.tag {
        case 0: // $5
            index = 0
            break
        case 1: // $10
            index = 1
            break
        case 2: // $50
            index = 2
            break
        default:
            print("DeveloperVC: Error with tipOptionClicked()")
        }
        let request = SKProductsRequest(productIdentifiers: ["com.r0cker.interstellar.tip1", "com.r0cker.interstellar.tip2", "com.r0cker.interstellar.tip3"])
        request.delegate = self
        request.start()
    }
    
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
        
        var validProduct: SKProduct? = nil
            let count = response.products.count
        print(count)
        switch index {
        case 0:
            if count > 0 {
                validProduct = response.products[0]
                print("Products Available!")
                purchase(validProduct)
            } else if validProduct == nil {
                print("No products available")
                //this is called if your product id is not valid, this shouldn't be called unless that happens.
            }
            break
        case 1:
            if count > 0 {
                validProduct = response.products[1]
                print("Products Available!")
                purchase(validProduct)
            } else if validProduct == nil {
                print("No products available")
                //this is called if your product id is not valid, this shouldn't be called unless that happens.
            }
            break
        case 2:
            if count > 0 {
                validProduct = response.products[2]
                print("Products Available!")
                purchase(validProduct)
            } else if validProduct == nil {
                print("No products available")
                //this is called if your product id is not valid, this shouldn't be called unless that happens.
            }
            break
        default:
            break
        }
            
    }
    
    func purchase(_ product: SKProduct?) {
        var payment: SKPayment? = nil
        if let product = product {
            payment = SKPayment(product: product)
        }

        SKPaymentQueue.default().add(self)
        SKPaymentQueue.default().add(payment!)
    }
    
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchasing:
                // Payment processing
                print("Transaction state -> Purchasing")
                break
            case .purchased, .restored:
                // Payment processed
                SKPaymentQueue.default().finishTransaction(transaction)
                print("Transaction state -> Purchased")
                break
            case .failed, .deferred:
                // Payment failed
                print("Transaction state -> Restored")
                        //add the same code as you did from SKPaymentTransactionStatePurchased here
                SKPaymentQueue.default().finishTransaction(transaction)
                break
            default:
                SKPaymentQueue.default().finishTransaction(transaction)
                SKPaymentQueue.default().remove(self)
                break
            }
        }
    }
}
